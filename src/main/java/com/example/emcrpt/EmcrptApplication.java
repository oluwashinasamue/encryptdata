package com.example.emcrpt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmcrptApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmcrptApplication.class, args);
    }

}
