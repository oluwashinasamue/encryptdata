package com.example.emcrpt;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/reports")
public class ReportController {
    private List<Report> reports = new ArrayList<>();

    //    @RequestMapping(value = "/reports",
//            method = RequestMethod.POST
////            ,
////            consumes = "text/report"
//    )
//    @ResponseBody
    @PostMapping(consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
            produces = {MediaType.APPLICATION_ATOM_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public String handleRequest(@Valid Report report, BindingResult result) {
//    public String handleRequest(WebRequest webRequest) {
        System.out.println("fasfasfad " + report.getReportName());
//        report.setId(reports.size() + 1);
//        reports.add(report);
        return "report saved: ";
    }

    @ResponseBody
    @PostMapping("/rest")
    public String handleRequest1(@RequestBody Report report) {
//    public String handleRequest(WebRequest webRequest) {
        System.out.println("fasfasfad " + report.getReportName());
//        report.setId(reports.size() + 1);
//        reports.add(report);
        return report.getReportName();
    }

    @GetMapping
    @ResponseBody
    public Report reportById(@PathVariable("id") int reportId) {
        if (reportId > reports.size()) {
            throw new RuntimeException("No found for the id :" + reportId);
        }
        return reports.get(reportId - 1);
    }
}
