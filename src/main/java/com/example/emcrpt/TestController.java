package com.example.emcrpt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.util.Locale;

/**
 * Created by SYLVESTER on 4/3/2017.
 */
@Controller
@RequestMapping("/form")
public class TestController {

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private ApplicationContext appContext;

    @GetMapping("/index")
    public String viewAccount( ) {
        return "test/form";

    }

    @PostMapping
    public String createCorpLocalBeneficiary(WebRequest webRequest) {
        final String username = webRequest.getParameter("reportName");
        System.out.println("username "+ username);
        final String sha256 = toSha256("ppp");
        System.out.println("post sha "+sha256);


        final String oldsha256 = oldetSHA256("ppp");
        System.out.println("post sha "+oldsha256);
        return "test/form";
    }
    private String toSha256(String input) {
        MessageDigest mDigest = null;
        try {
            mDigest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException var5) {
            var5.printStackTrace();
        }
        byte[] result = mDigest.digest(input.getBytes());
        StringBuffer sb = new StringBuffer();
        for(int i = 0; i < result.length; ++i) {
            sb.append(Integer.toString((result[i] & 255) + 256, 16).substring(1));
        }
        return sb.toString();
    }
    private String oldetSHA256(String originalString){
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] encodedhash = digest.digest(
                    originalString.getBytes());
            return bytesToHex(encodedhash);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return originalString;
    }
    private static String bytesToHex(byte[] hash) {
        StringBuilder hexString = new StringBuilder(2 * hash.length);
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if(hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }
}
