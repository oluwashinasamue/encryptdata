package com.example.emcrpt.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@Configuration
//@EnableWebMvc
//@ComponentScan
public class AppConfig {

//    @Override
//    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
//        converters.add(new ReportConverter());
//    }
    @Bean
    public HttpMessageConverters customConverters() {
        HttpMessageConverter<Object> converter = new TextJavascriptConverter(new ObjectMapper());
        return new HttpMessageConverters(Collections.singletonList(converter));
    }

    @Bean
    public ApplicationRunner demoRunner(RestTemplateBuilder builder, TextJavascriptConverter javascriptConverter) {
        return args -> {
            //can autowire RestTemplateBuilder for sensible defaults (including converters)
            RestTemplate restTemplate = builder.build();

            //confirm your converter is there
            if (restTemplate.getMessageConverters().contains(javascriptConverter)) {
                System.out.println("My custom HttpMessageConverter was registered!");
            }
        };
    }
}
