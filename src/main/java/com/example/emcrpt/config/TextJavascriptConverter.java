package com.example.emcrpt.config;

import com.example.emcrpt.Report;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.json.AbstractJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
@Component
public class TextJavascriptConverter extends AbstractJackson2HttpMessageConverter {

    public TextJavascriptConverter(ObjectMapper objectMapper) {
        super(objectMapper);
        setTextJavascriptAsSupportedMediaType();
    }
    public TextJavascriptConverter() {
        //can use overloaded constructor to set supported MediaType
        super(new ObjectMapper(), new MediaType("text", "javascript"));
    }
    private void setTextJavascriptAsSupportedMediaType() {
        List<MediaType> supportedMediaTypes = new ArrayList<>();
        supportedMediaTypes.add(new MediaType("text", "javascript"));
        setSupportedMediaTypes(supportedMediaTypes);
    }
}